fun is_older (date : (int * int * int), date2 : (int * int * int)) =
    (#1 date < #1 date2 orelse  #2 date < #2 date2) orelse #3 date < #3 date2

fun number_in_month (dates : (int * int * int) list, month : int) =
    if null dates
    then 0
    else if #2 (hd dates) = month
    then 1 + number_in_month (tl dates, month)
    else number_in_month (tl dates, month)

fun number_in_months (dates : (int * int * int) list, month : int list) =
    if null month
    then 0
    else number_in_month (dates, hd month) + number_in_months (dates, tl month)

fun dates_in_month (dates : (int * int * int) list, month : int) =
    if null dates
    then []
    else if #2 (hd dates) = month
    then (hd dates) :: dates_in_month((tl dates), month)
    else dates_in_month ((tl dates), month)

fun dates_in_months (dates : (int * int * int) list, month : int list) =
    if null month
    then []
    else dates_in_month (dates, hd month) @ dates_in_months (dates, tl month)

fun get_nth (el : string list, num : int) =
    if num=1
    then hd el
    else get_nth (tl el, num-1)

fun date_to_string (xs : (int * int * int)) =
    get_nth(["January", "February", "March", "April", "May", "June", "July",
             "August", "September", "October", "November", "December"], #2 xs) ^
    " " ^ Int.toString (#3 xs) ^ ", " ^ Int.toString (#1 xs)
    
(* implemented after deadline - doesn't count *)
fun number_before_reaching_sum (sum : int, lst : int list) =
    if sum <= hd lst
    then 0
    else 1 + number_before_reaching_sum(sum - hd lst, tl lst)

fun what_month (day_of_year : int) = 
    let 
        val month_lengths = [31,28,31,30,31,30,31,31,30,31,30,31]
    in
        1 + number_before_reaching_sum(day_of_year, month_lengths)
    end 

fun month_range (day1 : int, day2 : int) =
    if day1 > day2
    then []
    else what_month day1 :: month_range(day1 + 1, day2) 

fun oldest (xs : (int * int * int) list) =
    if null dates
    then NONE
    else let val ans = oldest(tl dates)
         in if isSome ans andalso is_older(valOf ans, hd dates)
            then ans
            else SOME (hd dates)
         end 
