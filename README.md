edu
===
MOOC files (coursera.org, edx.org, udacity.com, etc.)

<strong>aiippython</strong> - An Introduction to Interactive Programming in Python (coursera.org) <br>
<strong>mit600</strong> - 6.00x: Introduction to Computer Science and Programming (edx.org) <br>
<strong>netw</strong> - Introduction to Computer Networks (coursera.org) <br>
<strong>proglang</strong> - Programming Languages (coursera.org) <br>
