(function () {
    var allQuestions = [{
        question: "Which company first implemented the JavaScript language?",
        choices: ["Microsoft", "Sun Microsystems", "Netscape Communications"],
        correctAnswer: 2
    }, {
        question: "Which of the following browsers was the first to support JavaScript?",
        choices: ["Microsoft Internet Explorer", "Netscape Navigator", "Opera"],
        correctAnswer: 1
    }, {
        question: "The JavaScript international standard is called",
        choices: ["ECMA-262 Standard", "JavaScript 1.3 Standard", "IEEE-262 Standard"],
        correctAnswer: 0
    }];

    var questionCount = 0,
        score = 0,
        answers = [],
        allQuestionsLen = allQuestions.length,
    // initialize DOM elements
        doc = document,
        div = doc.getElementById("wrapper"),
        loginForm = doc.getElementById("loginForm"),
        userName = doc.getElementById("userName"),
        userPassword = doc.getElementById("userPassword"),
        loginButton = doc.getElementById("login"),
        questionHeader = doc.getElementById("questionHeader"),
        quizForm = doc.getElementById("quizForm"),
        backButton = doc.getElementById("back"),
        nextButton = doc.getElementById("next"),
    // create additional elements
        startButton = doc.createElement("button");

    // textContent is not supported in IE
    startButton.textContent = "Start Quiz",
    startButton.id = "start",
    startButton.type = "button";

    // Set header and answer choices

    function setAnswer(idEl, questionNum, choiceNum) {
        var choiceEl = doc.getElementById(idEl);
        choiceEl.innerHTML = allQuestions[questionNum].choices[choiceNum];
    }


    function showQuestion() {
        if (questionCount <= allQuestionsLen - 1) {
            questionHeader.innerHTML = allQuestions[questionCount].question;
            setAnswer("answ1", questionCount, 0);
            setAnswer("answ2", questionCount, 1);
            setAnswer("answ3", questionCount, 2);
        }
    }


    function updateScore() {
        for (var i = allQuestionsLen - 1; i >= 0; i--) {
            // use double equal to convert answers value to integer
            if (allQuestions[i].correctAnswer == answers[i].value) {
                score++;
            }
        }
    }


    function showScore() {
        updateScore();
        // show score
        questionHeader.innerHTML = "Your score is " + score;
        // remove quiz from page
        quizForm.style.display = "none";
        // add a Start Quiz button
        div.appendChild(startButton);
        startButton.addEventListener("click", function () {
            // reset quiz
            div.removeChild(startButton);
            questionCount = 0;
            score = 0;
            answers = [];
            showQuestion();
            quizForm.style.display = "block";
        }, false);
        return 0;
    }

    // select radiobutton group to uncheck button in the next question

    function getCheckedRadio(radioGroupName) {
        var buttons = doc.getElementsByName(radioGroupName);
        for (var i = buttons.length - 1; i >= 0; i--) {
            if (buttons[i].checked) {
                return buttons[i];
            }
        }
        return false;
    }


    function nextQuestion() {
        // cache checked button
        var currentButton = getCheckedRadio("choice");
        // cache chosen answers    
        answers[questionCount] = currentButton;
        if (currentButton.checked && questionCount <= allQuestionsLen) {
            questionCount++;
            showQuestion();
            currentButton.checked = false;
            // if current question was answered before check it
            if (answers[questionCount]) {
                answers[questionCount].checked = true;
            }
            if (questionCount === allQuestionsLen) {
                showScore();
            }
        } else {
            return false;
        }
    }


    function previousQuestion() {
        if (questionCount === 0) {
            return false;
        } else {
            questionCount--;
            showQuestion();
            answers[questionCount].checked = true;
        }
    }

    // authentication

    function loginUser() {
        userName = userName.value;
        userPassword = userPassword.value;
        if (userName && userPassword) {
            localStorage.setItem("name", userName);
            localStorage.setItem("password", userPassword);
            // cookie
            //doc.cookie = "name=" + userName;
            //doc.cookie = "password=" + userPassword;
            loginForm.innerHTML = "<h4>Welcome " + userName + "</h4>";
        }

    }


    function checkAuthentication() {
        // if user is logged in, display greeting
        if (localStorage.name !== undefined && localStorage.password !== undefined) {
            loginForm.innerHTML = "<h4>Welcome " + localStorage.name + "</h4>";
        }
    }


    function startQuiz() {
        // check if user is logged in
        //checkAuthentication();
        // show first question
        showQuestion();
    }

    // add event listener

    nextButton.addEventListener("click", nextQuestion, false);
    backButton.addEventListener("click", previousQuestion, false);
    //loginButton.addEventListener("click", loginUser, false);

    // play!
    startQuiz();
}).call(this)

// TODO, add login
// working version http://jsfiddle.net/alxers/ZC3JM/
