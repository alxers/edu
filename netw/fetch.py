import urllib,urllib2,sys,time

if len(sys.argv) < 2:
    print "Expected Argument: URL [File Prefix]"
    sys.exit()
    
fetchurl=sys.argv[1]

if len(sys.argv) > 2:
    prefix=sys.argv[2].strip()
else:
    prefix="html"

#Three urls, corresponding to direct download, direct via transparent proxy, and download via SSL proxy
urls=[fetchurl,"http://cs461coursera.appspot.com/proxy?url="+fetchurl,"https://cs461coursera.appspot.com/proxy?url="+fetchurl]


index=0
pages=[]
for url in urls:
    filename=prefix+str(index)+".html"
    start=time.time()
    html=urllib2.urlopen(url).read();
    pages.append(html)
    elapsed=time.time()-start
    print filename+" took "+str(elapsed)+" time to download "+str(len(html))+" bytes"
    f=open(filename,"w")
    f.write(html)
    f.close()
    index = index+1

#Send data to server
ip=urllib2.urlopen('http://icanhazip.com').read().strip()
sendurl='http://cs461coursera.appspot.com/store'
values={ 'url':fetchurl,
         'page0':pages[0],
         'page1':pages[1],
         'page2':pages[2],
         'userinfo':ip
         }
data=urllib.urlencode(values)
#print data
req=urllib2.Request(sendurl,data)
response=urllib2.urlopen(req)
print "Uploaded"
