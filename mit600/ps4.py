def getWordScore(word, n):
    """
    Returns the score for a word. Assumes the word is a valid word.

    The score for a word is the sum of the points for letters in the
    word, multiplied by the length of the word, PLUS 50 points if all n
    letters are used on the first turn.

    Letters are scored as in Scrabble; A is worth 1, B is worth 3, C is
    worth 3, D is worth 2, E is worth 1, and so on (see SCRABBLE_LETTER_VALUES)

    word: string (lowercase letters)
    n: integer (HAND_SIZE; i.e., hand size required for additional points)
    returns: int >= 0
    """
    w = 0 
    if len(word) == n:
        w += 50

    for letter in word:
        w += SCRABBLE_LETTER_VALUES[letter] * len(word)

    return w


def updateHand(hand, word):
    """
    Assumes that 'hand' has all the letters in word.
    In other words, this assumes that however many times
    a letter appears in 'word', 'hand' has at least as
    many of that letter in it. 

    Updates the hand: uses up the letters in the given word
    and returns the new hand, without those letters in it.

    Has no side effects: does not modify hand.

    word: string
    hand: dictionary (string -> int)    
    returns: dictionary (string -> int)
    """
    myHand = hand.copy()
    for i in word:
        myHand[i] = myHand.get(i, 0) - 1 
    return myHand


def isValidWord(word, hand, wordList):
    """
    Returns True if word is in the wordList and is entirely
    composed of letters in the hand. Otherwise, returns False.

    Does not mutate hand or wordList.
   
    word: string
    hand: dictionary (string -> int)
    wordList: list of lowercase strings
    """
    count = ''
    myHand = hand.copy()
    for i in word:
        myHand[i] = myHand.get(i, 0) - 1 
        if myHand[i] >= 0 and word in wordList:
            count += i
    return count == word


def calculateHandlen(hand):
    """ 
    Returns the length (number of letters) in the current hand.
    
    hand: dictionary (string int)
    returns: integer
    """
    s = 0
    for i in hand:
        s += hand[i]
    return s


def playGame(wordList):
    """
    Allow the user to play an arbitrary number of hands.
 
    1) Asks the user to input 'n' or 'r' or 'e'.
      * If the user inputs 'n', let the user play a new (random) hand.
      * If the user inputs 'r', let the user play the last hand again.
      * If the user inputs 'e', exit the game.
      * If the user inputs anything else, tell them their input was invalid.
 
    2) When done playing the hand, repeat from step 1
    """
    cont = True
    played = False
 
    while cont == True:
        choicelist = ("r","n","e")
        choice = raw_input("Enter n to play a new hand, r to replay the last hand or e to exit the game: ")
        lasthand = []
        if choice not in choicelist:
            print "Invalid Command."
        else:
            if choice == "n":
                hand = dealHand(HAND_SIZE)
                lasthand = hand.copy()
                played = True
                playHand(hand,wordList,HAND_SIZE)
                playGame(wordList)
            elif choice == "r":
                if played == True:
                    playHand(lasthand,wordList,HAND_SIZE)
                    playGame(wordList)
                else:
                    print "You have not played a hand yet! Please play a hand first! \n"
            elif choice == "e":
                cont = False
                break


def compChooseWord(hand, wordList):
    bestscore=0
    score=0
    bestword=None
    
    for words in wordList:
         if isValidWord(words, hand, wordList):
            score=getWordScore(words,sum(hand.values()))
            if score>bestscore:
                bestcore=score
                bestword=words
                
            
    return bestword



