i = 1 
total = 0 
while i < 13: 
    print 'Month: ' + str(i)
    total = total + (balance * monthlyPaymentRate)
    print 'Minimal monthly payment: ' + str(round(balance * monthlyPaymentRate, 2))
    balance = (balance - balance * monthlyPaymentRate) * (1 + annualInterestRate / 12) 
    print 'Remaining balance: ' + str(round(balance, 2))
    i += 1
print 'Total paid: ' + str(round(total, 2))
print 'Remaining balance: ' + str(round(balance, 2))


month = 0 
reset_balance = balance
min_month_pay = 10
while balance > 0:
    if month == 12: 
        month = 0 
        min_month_pay += 10
        balance = reset_balance
    balance = (balance - min_month_pay) * (1 + annualInterestRate / 12) 
    month += 1
print 'Lowest Payment: ' + str(min_month_pay)


mir = annualInterestRate / 12
low = balance/12
high = (balance*(1 + mir)**12)/12
epsilon = 0.01
mp = (high + low)/2.0
ob = balance
while abs(balance) >= epsilon:
    balance = ob
    for month in range(0, 12):
        balance = (balance - mp) * (1+mir)
    if  balance > 0:
        low = mp
    else:
        high = mp
    mp = (high + low)/2.0
else: print "Lowest Payment: " + str(round(mp, 2))
