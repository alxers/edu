import string

def buildCoder(shift):
    """
    Returns a dict that can apply a Caesar cipher to a letter.
    The cipher is defined by the shift value. Ignores non-letter characters
    like punctuation, numbers, and spaces.

    shift: 0 <= int < 26
    returns: dict
    """
    ### TODO 
    al_up = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']
    al_dow = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z']

    dic_up = {}
    dic_dow = {}
    newdic = {}

    for i in range(0, len(al_up)):
        dic_up[al_up[i]] = al_up[(i + shift) % len(al_up)]

    for i in range(0, len(al_dow)):
        dic_dow[al_dow[i]] = al_dow[(i + shift) % len(al_dow)]

    newdic.update(dic_up)
    newdic.update(dic_dow)

    return newdic



import string

def applyCoder(text, coder):
    """
    Applies the coder to the text. Returns the encoded text.

    text: string
    coder: dict with mappings of characters to shifted characters
    returns: text after mapping coder chars to original text
    """
    s = ''
    for letter in text:
        if letter in coder:
            s += coder[letter]
        else:
            s += letter

    return s



import string

def applyShift(text, shift):
    """
    Given a text, returns a new text Caesar shifted by the given shift
    offset. Lower case letters should remain lower case, upper case
    letters should remain upper case, and all other punctuation should
    stay as it is.

    text: string to apply the shift to
    shift: amount to shift the text (0 <= int < 26)
    returns: text after being shifted by specified amount.
    """
    ### TODO.
    ### HINT: This is a wrapper function.
    return applyCoder(text, buildCoder(shift))


import string

def findBestShift(wordList, text):
    """
    Finds a shift key that can decrypt the encoded text.

    text: string
    returns: 0 <= int < 26
    """
    i = 0 
    t = text.split(' ')
    while i < 26: 
        
        for word in t:
            shiftedWord = applyShift(word, i)  
            if isWord(wordList, shiftedWord) == True:
                return i
        i += 1


import string

def decryptStory():
    """
    Using the methods you created in this problem set,
    decrypt the story given by the function getStoryString().
    Once you decrypt the message, be sure to include as a comment
    your decryption of the story.

    returns: string - story in plain text
    """
    ### TODO
    #w = wordList()
    msg = getStoryString()
    shift = findBestShift(loadWords(), msg)
    
    return applyShift(msg, shift)
    #'Jack Florey is a mythical character created on the spur of a moment\nto help cover an insufficiently planned hack. He has been registered\nfor classes at MIT twice before, but has reportedly never passed a\nclass. It has been the tradition of the residents of East Campus to\nbecome Jack Florey for a few nights each year to educate incoming\nstudents in the ways, means, and ethics of hacking.\n'


def reverseString(aStr):
    """
    Given a string, recursively returns a reversed copy of the string.
    For example, if the string is 'abc', the function returns 'cba'.
    The only string operations you are allowed to use are indexing,
    slicing, and concatenation.
    
    aStr: a string
    returns: a reversed string
    """
    if len(aStr) <= 1:
        return aStr
    return aStr[-1:] + reverseString(aStr[:-1])


def x_ian(x, word):
    """
    Given a string x, returns True if all the letters in x are
    contained in word in the same order as they appear in x.
    
    x: a string
    word: a string
    returns: True if word is x_ian, False otherwise
    """
    if len(x) == 0:
        return True
    if len(x) > 0 and x[0] not in word:
        return False

    return x_ian(x[1:], word[word.find(x[:1]):]) 


def insertNewlines(text, lineLength):
    """
    Given text and a desired line length, wrap the text as a typewriter would.
    Insert a newline character ("\n") after each word that reaches or exceeds
    the desired line length.

    text: a string containing the text to wrap.
    lineLength: the number of characters to include on a line before wrapping
        the next word.
    returns: a string, with newline characters inserted appropriately. 
    """
    def insertNewlines(text, lineLength):
        if len(text) < lineLength:
            return text
        else:
            newtext = text[text[lineLength-1:].find(' ') + lineLength:]
            return text[:text[lineLength-1:].find(' ') + lineLength] + '\n' + insertNewlines(newtext, lineLength)
