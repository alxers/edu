from django.http import HttpResponse
import datetime

def hello(request):
    now = datetime.datetime.now()
    html = "<html>%s</html>" % now
    return HttpResponse(html)
