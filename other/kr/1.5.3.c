#include <stdio.h>
/*
main() {
    int c, nl;
    nl = 0;

    while( (c = getchar()) != EOF )
        if (c=='\n')
            ++nl;
    printf("%d\n", nl);
}

#offtop

main () {
    int c, a;
    c = 'A';
    a = "A";
    printf("%d\n", a);
}


#1.8

main() {
    int c, nl;
    nl = 0;
    while( (c = getchar()) != EOF  )
        if (c==32)
            ++nl;
    printf("%d\n", nl);
}


////???
//
main() {
    int c, nl;
    nl = 0;
    while( (c = getchar()) != EOF  )
    {
        if(c == 32)
        {
            if(nl == 0)
            {
                putchar(c);
                ++nl;
            }
        }

        if(c != 32)
        {
            putchar(c);
            nl = 0;
        }


    }
}
*/

//Ex.1.10

#include <stdio.h>
  
   main() {
   	int c,nl;
  
   	while( (c = getchar()) != EOF )
   	{
       	nl = 0;
  
     	if(c == '\t')
      	{
          	printf("\\t");
          	nl = 1;
      	}
 
      	if(c == '\b')
      	{
          	printf("\\b");
          	nl = 1;
      	}
 
      	if(c == '\\')
      	{
          	printf("\\\\");
          	nl = 1;
      	}
 
      	if(nl == 0)
      	{
          	putchar(c);
      	}
  	}
  }


