/*
#include <stdio.h>

#define IN 1
#define OUT 0

main() {
	int c, nl, nw, nc, state;

	state = OUT;
	nc = nl = nw = 0;

	while( (c = getchar()) != EOF ) {
    	++nc;
    	if (c == '\n')
        	++nl;
    	if (c == ' ' || c == '\n' || c == '\t')
        	state = OUT;
    	else if (state == OUT) {
        	state = IN;
        	++nw;
    	}
	}
	printf("%d %d %d\n", nc, nw, nl);

*/

//1.12


main()
{
  int c;
  int state;

  state = 0;
  while((c = getchar()) != EOF)
  {
	if(c == ' ' || c == '\t' || c == '\n')
	{
  	if(state == 0)
  	{
    	state = 1;
    	putchar('\n');
  	}
  	
	}
	else
	{
  	state = 0;
  	putchar(c);
	}
  }

}

