# include <stdio.h>
/*
main() {
    long nc;
    nc = 0;
    while ( getchar() != EOF )
        ++nc;
    printf("%ld\n", nc);
}

main() {
    int c;
    c = getchar();
    printf("%c", c);
}

*/

main() {
    double nc;
    for( nc=0; getchar() != EOF; ++nc )
        ;
    printf("%0.f\n", nc);
}
