#include <stdio.h>


int power(int m, int n);

main()
{
    int i;
    for (i = 0; i < 10; ++i)
        printf("%d %d %d\n", i, power(2,i), power(-3,i));
    return 0;
}

int power(int base, int n)
{
    int i, p;
    p = 1;
    for (i = 1; i <= n; ++i)
        p = p * base;
    return p;
}

//Ex.1.15

/*
float cel_to_f(float f);

main()
{
    float fahr;
    int lower, upper, step;

    lower = 0;
    upper = 300;
    step = 20;

    printf("Header\n");

    fahr = lower;
    while (fahr <= upper) {
        printf("%3.0f %6.1f\n", fahr, cel_to_f(fahr));
        fahr += step;
    }
    return 0;
}

float cel_to_f(float f)
{
    float cel;
    cel = (5.0 / 9.0) * (f - 32.0);
    return cel;
}
*/
