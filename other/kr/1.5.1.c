#include <stdio.h>

/*
main() {
    
    int c;

    c = getchar();
    while ( c != EOF ) {
        putchar(c);
        c = getchar();
    }
}

#Refactor

main() {
  
  int c;

  while ( (c = getchar()) != EOF )
      putchar(c);
}

#Ex.1.6

main() {
    printf("%d", (getchar() != EOF) );    
}

*/

main() {
    printf("%d\n", EOF);
}
