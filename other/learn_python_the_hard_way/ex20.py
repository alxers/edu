from sys import argv

script, input_file = argv

# define func that prints given file (content)
def print_all(f):
    print f.read()

# define func that sets pointer to the beginning of the file
def rewind(f):
    f.seek(0)

# prints a line number and corresponding line
def print_a_line(line_count, f):
    print line_count, f.readline()

# opens file from argument, and assign it to the current_file
current_file = open(input_file)

print "First let's print the whole file:\n"

# prints the whole file
print_all(current_file)

print "Now let's rewind, kind of like a tape."

# call rewind -> sets pointer to the beginning
rewind(current_file)

print "Let's print three lines:"

current_line = 1
# prints first line
print_a_line(current_line, current_file)

current_line += 1
# prints second line, current_line == 2
print_a_line(current_line, current_file)

current_line += 1
# prints third line, current_line == 3
print_a_line(current_line, current_file)


#What is a seek
# when you open a file, the system points to the beginning of the file.
# Any read or write you do will happen from the beginning.
# A seek() operation moves that pointer to some other part of the file
# so you can read or write at that place

# So, if you want to read the whole file but skip first 20 bytes, 
# open the file, seek(20) to move to where you want to start reading,
# then continue with reading the file

# Or say you want to read every 10th byte, you could write a loop
# that does seek(9,1) (moves 9 bytes forward
# relative to the current positions), reads one byte, repeat.
