print "Mary had a little lamb."
# printing with interpolation and insert a string 'snow'
print "Its fleece was white as %s." % 'snow'
# multiply "." 10 times so its printed 10 times
print "." * 10 # whoa, whait what?

end1 = "C"
end2 = "h"
end3 = "e"
end4 = "e"
end5 = "s"
end6 = "e"
end7 = "B"
end8 = "u"
end9 = "r"
end10 = "g"
end11 = "e"
end12 = "r"

# comma at the end, try to remove
# without comma it's printed 
#cheese
#burger
# without -> Cheese burger
print end1 + end2 + end3 + end4 + end5 + end6,
print end7 + end8 + end9 + end10 + end11 + end12
