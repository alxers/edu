numbers = []

def loop_f(num, inc):
    i = 0

    while i < num:
        print "At the top i is %d" % i
        numbers.append(i)

        i += inc
        print "Numbers now: ", numbers
        print "At the bottom i is %d" % i

def for_loop_f(num):
    for i in range(num):
        print "At the top i is %d" % i
        numbers.append(i)
        print "At the middle i is %d" % i

        print "Numbers now: ", numbers
        print "At the bottom i is %d" % i

#i = 0
#numbers = []
#
#while i < 6:
#    print "At the top i is %d" % i
#    numbers.append(i)
#
#    i = i + 1
#    print "Numbers now: ", numbers
#    print "At the bottom i is %d" % i

#loop_f(6, 1)
for_loop_f(6)

print "The numbers: "

for num in numbers:
    print num
