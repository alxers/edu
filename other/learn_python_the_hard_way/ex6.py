# assign to x var string "There are 10 types of...
x = "There are %d types of people." % 10

binary = "binary"
do_not = "don't"

# same
y = "Those who know %s and those who %s." % (binary, do_not)

print x
print y

# printing I said and interpolate x var in the %r placeholder(?)
print "I said: %r." % x

# printing '%s' is the same to %r here
print "I also said: '%s'." % y

hilarious = False
# assign to the joke var "Isn't that joke so funny?! %r
# so we can put after it some value (str interpolation)
joke_evaluation = "Isn't that joke so funny?! %r"

#putting value
print joke_evaluation % hilarious

w = "This is the left side of..."
e = "a string with a right side."

#cat!
print w + e
