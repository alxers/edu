print "Let's practice everything."
print 'You\'d need to know \'bout escapes with \\ that do \n newlines \t tabs.'

poem = """
\tThe lovely world
with logic so firmly planted
cannot discern \n the needs of love
nor comprehend passion from intuition
and requires an explanation
\n\t\twhere there is none.
"""

print "--------------"
print poem
print "--------------"

five = 10 - 2 + 3 - 6
print "This should be five: %s" % five

def secret_formula(started):
# put here argument and multiply by 500
    jelly_beans = started * 500
    # put here result from above and divide by 1000
    jars = jelly_beans / 1000
    # put here result from above and divide by 100
    crates = jars / 100
    # return 1, 2, 3 
    return jelly_beans, jars, crates

start_point = 10000
# multiple assingment or "unpacking"
# so beans == first return from function, jars == second return and so on
beans, jars, crates = secret_formula(start_point)

print "With a starting point of: %d" % start_point
print "We'd have %d beans, %d jars, and %d crates." % (beans, jars, crates)

start_point = start_point / 10

print "We can also do that this way:"
# the same here, returned values from secret_formula appear in the printed string
print "We'd have %d beans, %d jars, and %d crates." % secret_formula(start_point)
