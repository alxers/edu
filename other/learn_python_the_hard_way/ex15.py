#imports from module sys this thing - argv
from sys import argv

#assign to the var script - scriptname, to the filename first argument
#script, filename = argv

#assign to the txt opened file (returns the file object (whole file?))
#IT'S LIKE DVD, you can move it inside the player, and then "read"!!!
#txt = open(filename)

#prints filename
#print "Here's your file %r:" % filename
#reads the file?
#print txt.read()

#asks to enter the name of the file
print "Type the filename againg:"
#assign to the file_again the entered input from user
file_again = raw_input("> ")

#assign to the txt_again file object
txt_again = open(file_again)

#prints this file
print txt_again.readlines()
txt_again.close()
